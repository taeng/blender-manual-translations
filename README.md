# Blender Documentation Translation

Welcome to the project for managing translations for the
([Blender User Documentation](https://docs.blender.org/manual/en/dev/)).
We are always looking for more translators!

The Blender Manual itself is written using `reStructuredText` (RST)
and is built with [Sphinx](http://www.sphinx-doc.org/en/stable/).

Translations are managed using the popular gettext Internationalization and localization system.
This means that strings are extracted to po-files which provide a mapping for translated strings.

If you want to start contributing it is important to first understand how the English manual works.

## How to build & edit the docs locally

Before you start contributing it may be helpful to read the style guides
which give instructions on how to write with RST and some goals of the manual.

- [Markup Style Guide](https://docs.blender.org/manual/en/dev/contribute/guides/markup_guide.html)
- [Writing Style Guide](https://docs.blender.org/manual/en/dev/contribute/guides/writing_guide.html)

Now that you are familiar with the process, you can get setup,
edit, and contribute your changes by following these links:

- [Install](https://docs.blender.org/manual/en/dev/contribute/install/index.html)
- [Build](https://docs.blender.org/manual/en/dev/contribute/build.html)
- [Edit](https://docs.blender.org/manual/en/dev/contribute/editing.html)
- [Build Again](https://docs.blender.org/manual/en/dev/contribute/build.html)
- [Open Pull Request](https://docs.blender.org/manual/en/dev/contribute/pull_requests.html)

# How to translate the manual

For translations, we use Sphinx’s internationalization package.
To get started see the [contribution guide for translations](https://docs.blender.org/manual/en/dev/contribute/index.html#translations).


## Links

- **Source Files**: [Translation's Repository](https://projects.blender.org/blender/blender-manual-translations) (git repository).
- **Forum**: [Documentation category on Developer Talk](https://devtalk.blender.org/c/documentation)
- **Chat**: [#translations on blender.chat](https://blender.chat/channel/translations)
- **Administrators**: @blendify , @fsiddi

**Note:*
If you are a new translator who wants to start translating a new language that is not listed in this repository, please get in touch on the [#translations chat](https://blender.chat/channel/translations), and we will be glad to set it up for you.


## Issues

Translation issues can be reported [here](https://projects.blender.org/blender/blender-manual/issues/new?template=.gitea%2fissue_template%2fbug.yaml).
